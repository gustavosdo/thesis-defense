\beamer@sectionintoc {1}{LHC \& LHCb Run 1}{3}{0}{1}
\beamer@sectionintoc {2}{CEP of dimuons in $pp$ collisions}{13}{0}{2}
\beamer@sectionintoc {3}{Coherent $J/\psi $ production in PbPb collisions}{36}{0}{3}
\beamer@sectionintoc {4}{HeRSCheL}{50}{0}{4}
\beamer@sectionintoc {5}{Updated measurement of coherent $J/\psi $}{54}{0}{5}
\beamer@sectionintoc {6}{Conclusions}{68}{0}{6}
